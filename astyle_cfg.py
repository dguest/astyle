#!/usr/bin/env python3
#-----------------------------------------------------------------
#
# ATLAS Style Guide Checker
# astyle_cfg.py - user configuration options
#
# Copyright 2017-22 Steve Lloyd - Queen Mary University of London
# MIT License
#
#-----------------------------------------------------------------

from astyle_config import *

class Config(ConfigBase):
 
	# Override the default options here
	
	# Only run these tests
	
#	ConfigBase.tests = ['My Simple Test', 'My Regex Test', 'Phrases', 'Spelling']

	# Don't run these tests
	
#	ConfigBase.ignore_tests = ['Phrases', 'Spelling']

	# Define your own tests. 'case': True means expression is case-sensitive 
	
#	ConfigBase.test_data.append({'name': 'My Simple Test', 'run': True, 'function': 'findsimpleterm','case': False, 'terms': ['something I don\'t like'], 'desc': "Avoid using this phrase.", 'level': 'high', 'phase': 1})

#	ConfigBase.test_data.append({'name': 'My Regex Test', 'run': True, 'function': 'findregexterm', 'case': True, 'terms': [r'\b(something I don\'t like)\b'], 'desc': "Avoid using this phrase.", 'level': 'high', 'phase': 1})
	
	pass
