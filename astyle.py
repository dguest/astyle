#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------
#
# ATLAS Style Guide Checker
# astyle.py - Main program
#
# Contact: s.l.lloyd@qmul.ac.uk
#
# Some additional grammar rules taken from Neil Spring's style-check see:
# http://www.cs.umd.edu/~nspring/software/style-check-readme.html
#
# Copyright 2017-22 Steve Lloyd - Queen Mary University of London
# MIT License
#
#-----------------------------------------------------------------

import os
import re
import sys
import inspect
import argparse
import datetime
import hashlib
import functools

sys.path.insert(0,'.')

results = []
acronyms = {}
line_numbers = {}
references = []
file_numbers = {}
file_number = 0

#=================================================================
# Main Program
#=================================================================

def astyle_main ():
	global path
	
	print ('------------------------------------------------------')
	print ('Running astyle.py - version', Config.version, 'at', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
	print ('------------------------------------------------------')
	if Args.british:
		Config.british = True
	if Args.american:
		Config.british = False
	if Config.british:
		print ('Language set to British English')
	else:
		print ('Language set to American English')
	print ('Threshold set to', Args.threshold)
	print ()
	
	# Rearrange the test data
	
	test_len = len(Config.tests)
	for test in Config.test_data:
		Config.test_info[test['name']] = test
		if test['name'] in Config.ignore_tests: Config.test_info[test['name']]['run'] = False
		if test_len == 0: Config.tests.append(test['name'])
	
#	print Config.tests
	path = os.path.dirname(Args.input)	
	
	read_rules()
	read_file(Args.input)
	show_results()
	
#-----------------------------------------------------------------
		
def read_rules():

	# Reads the rules files

	reg = re.compile(r'^(.*?)\s+%\s*(\w+)\s*(.*)\s*')
	
	this_file = os.path.abspath(inspect.stack()[0][1]) 
	path = os.path.dirname(this_file)
#	print path
	rule_files = [path + '/' + Config.rules_file, Args.rules]
	for filename in rule_files:
		print ('Reading rules from', filename)
		try:
			f = open(filename)
			text = f.read()
			f.close()
		except:
			print ('Failed to open rules file', filename)
			continue
#		print text

		lines = text.splitlines()
		for line in lines:
			line.strip()
			if line == '': continue
			if line.startswith('#'): continue
			match = reg.match(line)
			if match:		
				trm = match.group(1)
				rule = match.group(2)
				suggest = match.group(3)
				suggest = suggest.replace('"', '\'')
				name = Config.rule_names[rule]
				if rule == 'english':
					Config.test_info[name]['aterms'].append(trm)
					Config.test_info[name]['bterms'].append(suggest)
				else:
					if rule == 'syntax':
						trm = '(' + trm + ')'
					if suggest != '': 
						term = (trm, suggest)
					else:
	#					print trm, ':', rule, ':', suggest
						term = trm
					Config.test_info[name]['terms'].append(term)
#				print term, ':', rule, ':', suggest
			else:
				print ('Unrecognised rule:')
				print (line)
			
#-----------------------------------------------------------------
		
def read_file(filename):

	# Reads a (LaTex) file
	
	global path, raw_text, line_numbers, file_numbers, file_number
	
	print ('Reading', filename)
	try:
		f = open(filename)
		raw_text = f.read()
		f.close()
	except:
		print ('Failed to open file', filename)
		return

	file = os.path.basename(filename)
	if file in Config.ignore_files: return
	
	if not file in file_numbers:
		file_numbers[file] = file_number
		file_number += 1
		
#	line_numbers = raw_text.splitlines()
	line_numbers = get_lines(raw_text)
	
#	print raw_text
	
	# Remove comments

	text = remove_comments(raw_text)
#	print text
#	print len(text), len(raw_text)
	
	# Look for \input{newfile} or \include{newfile]
	
	regi = re.compile('\\\\(?:input|include){(.*?)}')
	files = regi.findall(text)

	# Remove commands
	
	text = remove_commands(text)
#	print references
#	print text
#	print len(text), len(raw_text)
	
	# Parse the file
	
	parse_file(filename, text, 1)
	
	text = remove_equations(text)
#	print text
	
	parse_file(filename, text, 2)
	
	text = remove_tables(text)
	
	parse_file(filename, text, 3)
#	print text
#	print len(text), len(raw_text)
	
	find_acronyms(filename, text)
	
	# Read any other files found 
	
#	path = os.path.dirname(filename)	
	for newfile in files:
		if not newfile.endswith('.tex'): newfile += '.tex'
		read_file(os.path.join(path, newfile))	

#-----------------------------------------------------------------
	
def remove_commands(text):

	# Replaces anything inside various commands with spaces e.g \cite{bla bla} -> \cite{       }

#	output = ''
#	lines = text.splitlines()
#	for line in lines:
	for command in Config.ignored_latex_commands:
		# e.g \command{...}
		reg1 = re.compile(r'\\' + command + r'{(.+?)}')	
		# e.g \command[...]{...}
		reg2 = re.compile(r'\\' + command + r'\[(.+?)\]{(.+?)}')
		reg3 = re.compile(r'\\' + command + r'{(.+?)}{(.+?)}')	
		reg4 = re.compile(r'\\' + command + r'{(.+?)}{(.+?)}{(.+?)}')	
		matches = reg1.finditer(text)
		for match in matches:
#			print '1 Command', command, 'found:', match.start(), match.end(), match.group(1), match.group(0)
#			print 'before:', text[match.start()-2:match.end()+2]
			text = text[:match.start()] + '\\' + command + '{' + ' '*len(match.group(1)) + '}' + text[match.end():]
			 # Collect cites
			if command == 'cite' or command == 'autocite':
				cites = match.group(1).split(',')
				for cite in cites:
					cite.strip()
					references.append(cite)
		matches = reg2.finditer(text)
		if command == 'cite': continue
		for match in matches:
#			print '2 Command', command, 'found:', match.start(), match.end(), match.group(1), match.group(0)
#			print 'before:', text[match.start()-2:match.end()+2]
			text = text[:match.start()] + '\\' + command + '[' + ' '*len(match.group(1)) + ']' + '{' + ' '*len(match.group(2)) + '}' + text[match.end():]

		matches = reg3.finditer(text)
		for match in matches:
#			print '3 Command', command, 'found:', match.start(), match.end(), match.group(1), match.group(0)
#			print 'before:', text[match.start()-2:match.end()+2]
#			print text[match.start()-20:match.end()+20]
			text = text[:match.start()] + '\\' + command + '{' + ' '*len(match.group(1)) + '}' + '{' + ' '*len(match.group(2)) + '}' + text[match.end():]
			 		
		matches = reg4.finditer(text)
		for match in matches:
#			print '4 Command', command, 'found:', match.start(), match.end(), match.group(1), match.group(0)
#			print 'before:', text[match.start()-2:match.end()+2]
			text = text[:match.start()] + '\\' + command + '{' + ' '*len(match.group(1)) + '}' + '{' + ' '*len(match.group(2)) + '}'+ '{' + ' '*len(match.group(3)) + '}' + text[match.end():]
#			print 'after: ', text[match.start()-2:match.end()+2]
#				line = line.replace('\\' + command + '{' + match.group(1) + '}', '\\' + command + '{' + ' '*len(match.group(1)) + '}')
#				print line
		
#		output += line + '\n'

	# Speical case of \LEcomment{original}{comment} where original is kept but comment removed
		
	reg = re.compile(r'\\LEcomment{(.+?)\}{(.+?)\}')	
	matches = reg.finditer(text)
	for match in matches:
#		print match.start(), match.end(), match.group(1), match.group(0)
#		print 'before:', text[match.start()-4:match.end()+4]
		text = text[:match.start()] + '           ' + match.group(1) + '  ' + ' '*len(match.group(2)) + ' ' + text[match.end():]
#		print 'after: ', text[match.start()-4:match.end()+4]
	
#	print text
#	sys.exit(0)		
	return text
	
#-----------------------------------------------------------------
	
def remove_comments(text):

	# Replaces comments %... with spaces. Leaves the \n to preserve line numbers
	# Also \begin{comment} ... \end{comment}
	# Also replaces everything between \iffalse and \fi
	
	output = ''
	is_iffalse = False
	is_comment_env = False
	lines = text.splitlines()
	for line in lines:
		i = 0
		is_comment = False
#		if line.startswith('\\iffalse'): is_iffalse = True
#		if line.startswith('\\fi'): is_iffalse = False
		while i<len(line):
			if i <= (len(line)-len('\\iffalse')) and line[i:i+len('\\iffalse')] == '\\iffalse':  is_iffalse = True
			if i <= (len(line)-len('\\fi')) and line[i:i+len('\\fi')] == '\\fi':  is_iffalse = False
			if i <= (len(line)-len('\\begin{comment}')) and line[i:i+len('\\begin{comment}')] == '\\begin{comment}':  is_comment_env = True
			if i <= (len(line)-len('\\end{comment}')) and line[i:i+len('\\end{comment}')] == '\\end{comment}':  is_comment_env = False
			# Remove everything following a % unless it is \%
			if line[i] == '%' and (i == 0 or line[i-1] != '\\'): 
				is_comment = True

			if is_comment or is_comment_env or is_iffalse:
				output += ' '
			else:
				output += line[i]
			i += 1
				
		output += '\n'
		
	return output

#-----------------------------------------------------------------
	
def remove_equations(text):
	
	# Removes equations but leaves the space to preserve line numbers
	
	output = ''
	is_equation = False
	
#	print string
	i = 0
	starts = ['\\[', '\\(', '\\begin{math}', '\\begin{displaymath}', '\\begin{equation}', '\\begin{equation*}']
	ends = ['\\]', '\\)', '\\end{math}', '\\end{displaymath}', '\\end{equation}', '\\end{equation*}']

	while i<len(text):
		if text[i:i+2] == '$$':
			is_equation = not is_equation
#			print 'found $$', is_equation
			output += text[i:i+2]
			i += 2
			continue
		if text[i] == '$' and (i == 0 or text[i-1] != '\\'):
			is_equation = not is_equation
#			print 'found $', is_equation
			output += text[i]
			i += 1
			continue

		found = False
		for str in starts:
			if text[i:i+len(str)] == str:
				if text[i-1:i+2] == '\\\\[': continue
				is_equation = True
#				print 'found', string[i:i+len(str)], is_equation
				output += text[i:i+len(str)]
				i += len(str)
				found = True
				break
		for str in ends:
			if text[i:i+len(str)] == str:
				is_equation = False
#				print 'found', string[i:i+len(str)], is_equation
				output += text[i:i+len(str)]
				i += len(str)
				found = True
				break
		if found: continue
		
		if not is_equation or text[i] == '\n' or text[i] == ' ':
			output += text[i]
		else:
			output += ' '

		i += 1
			
#	output += '\n'
#	print '++++'
#	print output
#	print '++++'
		
	return output
	 		
#-----------------------------------------------------------------
	
def remove_tables(text):
	
	# Removes tables but leaves the space to preserve line numbers
	
	output = ''
	is_table = False
	
#	print string
	i = 0
	starts = ['\\begin{tabular}']
	ends = ['\\end{tabular}']

	while i<len(text):
		if text[i:i+2] == '$$':
			is_table = not is_table
#			print 'found $$', is_table
			output += text[i:i+2]
			i += 2
			continue
		if text[i] == '$' and (i == 0 or text[i-1] != '\\'):
			is_table = not is_table
#			print 'found $', is_table
			output += text[i]
			i += 1
			continue

		found = False
		for str in starts:
			if text[i:i+len(str)] == str:
				is_table = True
#				print 'found', string[i:i+len(str)], is_table
				output += text[i:i+len(str)]
				i += len(str)
				found = True
				break
		for str in ends:
			if text[i:i+len(str)] == str:
				is_table = False
#				print 'found', string[i:i+len(str)], is_table
				output += text[i:i+len(str)]
				i += len(str)
				found = True
				break
		if found: continue
		
		if not is_table or text[i] == '\n' or text[i] == ' ':
			output += text[i]
		else:
			output += ' '

		i += 1
			
#	output += '\n'
#	print '++++'
#	print output
#	print '++++'
		
	return output
#-----------------------------------------------------------------
	
def parse_file(filename, text, phase):

	# Parses the file
	
	file = os.path.basename(filename)
#	print 'Parsing', filename, file
	for test in Config.tests:
		if not Config.test_info[test]['run']: continue
		if not Config.test_info[test]['phase'] == phase: continue
		if Config.threshold_levels[Args.threshold] > Config.threshold_levels[Config.test_info[test]['level']]: continue
		if 'case' in Config.test_info[test]:
			case = Config.test_info[test]['case']
		else:
			case = False
		if Config.test_info[test]['function'] == 'findsimpleterm':
			for term in Config.test_info[test]['terms']:
				find_simple_term(file, text, test, term, case)
		elif Config.test_info[test]['function'] == 'findregexterm':
			for term in Config.test_info[test]['terms']:
				find_regex_term(file, text, test, term, case)
		elif Config.test_info[test]['function'] == 'findenglish':
			if Config.british:
				terms = Config.test_info[test]['aterms']	# Because the american ones are wrong
			else:
				terms = Config.test_info[test]['bterms']
			for term in terms:
				find_simple_term(file, text, test, term, [])
#		elif Config.test_info[test]['function'] == 'findacronyms':
#			find_acronyms(file, text)

#	print 'lines', num

#-----------------------------------------------------------------

def find_acronyms(filename, text):

	# Find acronyms - 2 or more capital letters e.g. ABC
	
	file = os.path.basename(filename)
	 
	# Don't look in the acknowledgements etc
	if file in Config.ignore_acronym_files: return
	
	# Ignore Acknowledgements text
	
	text = re.sub(r'We acknowledge the support.*?United Kingdom\.', '', text)
	text = re.sub(r'The crucial computing support.*?providers\.', '', text)
#	print text
#	sys.exit(0)
	# Find definitions - (ABC)
	
	reg1 = re.compile(r'\(([A-Z]{2,})s?\)')
	matches = reg1.finditer(text)
	for match in matches:		
		acro = match.group(1)
		if acro in Config.ignore_acronyms: continue
#		context = get_context(string, match.start(), match.end())
		context = get_context(raw_text, match.start(), match.end())
		line1 = -1
		line2 = -1
		for key in line_numbers:
			if key[0] <= match.start() <= key[1]:
				line1 = line_numbers[key]
#				break
			if key[0] <= match.end() <= key[1]:
				line2 = line_numbers[key]
#		if acro == 'LO' or acro == 'NNLO': print 'Defined', acro, file, line1, line2, context		
		if not acro in acronyms:
			acronyms[acro] = {'defined_first': True, 'defined': 0, 'count': 0, 'defined_line1': -1, 'defined_line2': -1, 'used_line1': -1, 'used_line2': -1}
		if acronyms[acro]['defined'] == 0:
			acronyms[acro]['defined_file'] = file
			acronyms[acro]['defined_line1'] = line1
			acronyms[acro]['defined_line2'] = line2
			acronyms[acro]['defined_context'] = context
		acronyms[acro]['count'] += 1
		acronyms[acro]['defined'] += 1

	# Remove things in \cite{...} etc
	
#	string2 = string
#	for term in Config.ignore_acronym_commands:
#		regi =  re.compile('\\\\' + term + r'\{.*?\}')
#		text = regi.sub('', text)
#	print text

	# Find usage - ABC
	
	reg2 = re.compile(r'\b(?<!\\)([A-Z]{2,})s?\b')
	matches = reg2.finditer(text)
	for match in matches:
#		print match.group(0), match.group(1)
		acro = match.group(1)
		if acro.startswith('('): continue		
		if acro in Config.ignore_acronyms: continue
		# Ignore things like MC@NLO and POWHEG-BOX
		if match.start() > 0 and text[match.start()-1] == '@': continue
		if match.start() > 0 and text[match.start()-1] == '-': continue
#		context = get_context(string, match.start(), match.end())
		context = get_context(raw_text, match.start(), match.end())
		line1 = -1
		line2 = -1
		for key in line_numbers:
			if key[0] <= match.start() <= key[1]:
				line1 = line_numbers[key]
#				break
			if key[0] <= match.end() <= key[1]:
				line2 = line_numbers[key]
#		if acro == 'LO' or acro == 'NNLO': print 'Found', acro, file, line1, line2, context		
		if not acro in acronyms:
			acronyms[acro] = {'defined_first': False, 'defined': 0, 'count': 0, 'defined_line1': -1, 'defined_line2': -1, 'used_line1': -1, 'used_line2': -1}
		acronyms[acro]['count'] += 1
		if acronyms[acro]['count'] == 1:
#			if acro == 'LO' or acro == 'NNLO': print 'count 1'		
			acronyms[acro]['used_file'] = file
			acronyms[acro]['used_line1'] = line1
			acronyms[acro]['used_line2'] = line2
			acronyms[acro]['used_context'] = context
		elif acronyms[acro]['defined_first'] and acronyms[acro]['defined_file'] == file and line1 < acronyms[acro]['defined_line1']:
#			if acro == 'LO' or acro == 'NNLO': print 'defined earlier'		
			acronyms[acro]['defined_first'] = False
			acronyms[acro]['used_file'] = file
			acronyms[acro]['used_line1'] = line1
			acronyms[acro]['used_line2'] = line2
			acronyms[acro]['used_context'] = context
			
#-----------------------------------------------------------------

def find_regex_term(file, text, test, tterm, case):

	# Find a regular expression term
	# case true is case sensitive

	if isinstance(tterm, tuple):
		term = tterm[0]
		suggest = tterm[1]
	else:
		term = tterm
		suggest = ''
	
#	print test, term
	if case:
		reg = re.compile(term)
	else:
		reg = re.compile(term, re.I)
		
	matches = reg.finditer(text)
	add_results(test, file, text, matches, suggest)

#-----------------------------------------------------------------

def find_simple_term(file, text, test, tterm, case):

	# Find a simple term - case insensitive
	# case true is case sensitive
	
	if isinstance(tterm, tuple):
		term = tterm[0]
		suggest = tterm[1]
	else:
		term = tterm
		suggest = ''

	if case:
		reg = re.compile(r'\b(' + term + r')\b')
	else:
		reg = re.compile(r'\b(' + term + r')\b', re.I)
		
	matches = reg.finditer(text)
	add_results(test, file, text, matches, suggest)

#-----------------------------------------------------------------

def add_results(test, file, text, matches, suggest):

	# Given a match adds it to the results
	
	for match in matches:
#		context = get_context(string, match.start(), match.end())
#		context = get_context(line_numbers[line-1], match.start(), match.end())
		if match.start() > 0 and text[match.start()-1] == '\\':
#			print 'Ignoring Latex command \\' + match.group(1)
			continue
		if match.start() > 7 and text[match.start()-7:match.start()] == '\\begin{' and match.end() <= len(text) and text[match.end()] == '}':
#			print 'Ignoring Latex command \\begin{' + match.group(1) + '}'
			continue
		if match.start() > 5 and text[match.start()-5:match.start()] == '\\end{' and match.end() <= len(text) and text[match.end()] == '}':
#			print 'Ignoring Latex command \\end{' + match.group(1) + '}'
			continue
		context = get_context(raw_text, match.start(), match.end())
#		context = context.replace('\n', ' ')
		found = match.group(1).replace('\n', ' ')
#		found += ' ' + str(match.start()) + ' ' + str(match.end())
		line1 = -1
		line2 = -1
		for key in line_numbers:
			if key[0] <= match.start() <= key[1]:
				line1 = line_numbers[key]
#				break
			if key[0] <= match.end() <= key[1]:
				line2 = line_numbers[key]
				
		result = {'test': test, 'file': file, 'line1': line1, 'line2': line2, 'found': found, 'context': context, 'suggest': suggest}
		results.append(result)

#-----------------------------------------------------------------

def get_context(text, start, end):

	# Gets the wider context of the text between characters start and end	

	context = text[start:end]

	# Look back to next punctuation or start of text
	# Stop at next space if more than a certain distance

	i = start
	n = 0
	while True:
		i -= 1
		if i == -1: break
		n += 1
		if text[i] == ' ' and n > Config.context_max:
			context = '...' + context 
			break
#		if text[i] in ['.', '?', '!', '~', ',']: break
#		if text[i] in ['.', '?', '!']: break
		context = text[i] + context

	if context[0] == ' ': context = context[1:]
	
	# Look forward to next punctuation or end of text
	# Stop at next space if more than a certain distance
	
	i = end - 1
	n = 0
	while True:
		i += 1
		if i == len(text): break
		n += 1
		if text[i] == ' ' and n > Config.context_max:
			context += '...'
			break
		context += text[i]
#		if text[i] in ['.', '?', '!', '~', ',']: break
#		if text[i] in ['.', '?', '!']: break

	context = context.replace('\n', ' ')
		
	return context
	
#-----------------------------------------------------------------

def get_lines(text):
	
	lines = {}
	start = 0
	end = -1
	n = 1
	i = 0
	while i<len(text):
		if text[i] == '\n':
			end = i
			lines[(start, end)] = n
			start = i+1
			end = -1
			n += 1
		i += 1
	if start < len(text) and end == -1:
		end = i-1
		lines[(start, end)] = n
		
#	print lines
	return lines
	
#-----------------------------------------------------------------

def show_results():

	# Show the results
	
	# Test results
	if Args.orderbylines:
		results.sort(key=functools.cmp_to_key(sort_results2))
	else:
		results.sort(key=functools.cmp_to_key(sort_results1))
	
	totals = {'low': 0, 'medium': 0, 'high': 0}
	last_test = ''
	last_file = ''
	for result in results:
#		hash_object = hashlib.sha1(result['file'] + result['context'])
#		string = result['file'] + result['context']
#		hash_object = hashlib.sha1(string.encode('utf-8'))
#		hex_dig = hash_object.hexdigest()
#		hash = hex_dig[:8]
#		if hash in Config.hash_ignores:
#			totals['ignored'] += 1
#			continue
		print ()
		if Config.test_info[result['test']]['level'] in totals:
			totals[Config.test_info[result['test']]['level']] += 1
		if not Args.orderbylines and result['test'] != last_test:
			last_file = ''
			underline = '-' * (len(result['test']) + len(Config.test_info[result['test']]['level']) + 10)
			print ('Check:', result['test'], '(' + str(Config.test_info[result['test']]['level']) + ')')
			print (underline)
#			if Args.verbose: print 'ATLAS Style Guide says', '"' + Config.test_info[result['test']]['desc'] + '"'
			if Args.verbose:
				desc = Config.test_info[result['test']]['desc'].replace('ASG ', 'The ATLAS Style Guide says - ')
				print (desc)
			print ()
			
		if result['line1'] == result['line2']:
			linestr = 'Line ' + str(result['line1'])
		else:
			linestr = 'Lines ' + str(result['line1']) + '-' + str(result['line2'])
			
		if result['file'] != last_file:
			last_file = result['file']
			print ('File:', result['file'])
			print ()
			
#		print  linestr, '[' + hash + ']'
		if result['suggest'] != '':
			suggest = ' Suggest: ' + result['suggest']
		else:
			suggest = ''
		
		print (linestr, '-', 'Found', '"' + result['found'] + '"', 'Context: "' + result['context'] + '"', suggest)
		last_test = result['test']
		
	# References
	
	if 'Standard References' in Config.tests and Config.test_info['Standard References']['run'] and Config.threshold_levels[Args.threshold] <= Config.threshold_levels[Config.test_info['Standard References']['level']]:
		missing = []
		for term_tuple in Config.standard_references:
			name = term_tuple[0]
			term = term_tuple[1]
			if term in references: continue
			missing.append(term_tuple)
		
		if len(missing) > 0:
		
			print ()
			print ('Standard References')
			print ('-------------------')
			if Args.verbose: print ('ATLAS Style Guide says -', Config.test_info['Standard References']['desc'])
			print ()
			for miss_tuple in missing:
				print ('Possibly missing', miss_tuple[1], '- \cite{' + miss_tuple[0] + '}')
				
	# Acronyms

	if 'Acronyms' in Config.tests and Config.test_info['Acronyms']['run'] and Config.threshold_levels[Args.threshold] <= Config.threshold_levels[Config.test_info['Acronyms']['level']]:
		print ()
		print ('Acronyms')
		print ('--------')
		if Args.verbose: print ('ATLAS Style Guide says -',  Config.test_info['Acronyms']['desc'])
		print ()
	
		for acronym in acronyms:
#			if acronym == 'LO' or acronym == 'NNLO': print acronym, acronyms[acronym]
			msg = 'unknown'
			if acronyms[acronym]['defined_line1'] == acronyms[acronym]['defined_line2']:
				defined_linestr = ' line ' + str(acronyms[acronym]['defined_line1'])
			else:
				defined_linestr = ' lines ' + str(acronyms[acronym]['used_line1']) + '-' + str(acronyms[acronym]['defined_line2'])
			if acronyms[acronym]['used_line1'] == acronyms[acronym]['used_line2']:
				used_linestr = ' line ' + str(acronyms[acronym]['used_line1'])
			else:
				used_linestr = ' lines ' + str(acronyms[acronym]['used_line1']) + '-' + str(acronyms[acronym]['used_line2'])
				
			if acronyms[acronym]['defined'] == 1:
				if acronyms[acronym]['defined_first']:
					if acronyms[acronym]['count'] > 1:
						msg = 'OK'
					else:
						msg = 'Only used once at '+ acronyms[acronym]['defined_file'] + defined_linestr + ' "' + acronyms[acronym]['defined_context'] + '"'
				else:
					msg = 'defined at ' + acronyms[acronym]['defined_file'] + defined_linestr + ' "' + acronyms[acronym]['defined_context'] + '" - first used ' + acronyms[acronym]['used_file'] + used_linestr + ' "' + acronyms[acronym]['used_context'] + '"'
			elif acronyms[acronym]['defined'] > 1:
				msg = 'defined more than once - first defined at ' + acronyms[acronym]['defined_file'] + defined_linestr + ' "' + acronyms[acronym]['defined_context'] + '"'
			else:
				msg = 'Not defined - first used ' + acronyms[acronym]['used_file'] + used_linestr + ' "' + acronyms[acronym]['used_context'] + '"'
		
			if acronyms[acronym]['count'] > 1:
				print (acronym, 'used', acronyms[acronym]['count'], 'times -', msg)
			else:
				print (acronym, 'used', acronyms[acronym]['count'], 'time -', msg)

	# Totals
					
	print ()
	print ('Total results found', '%4s' % len(results))
	print ('High threshold     ', '%4s' % totals['high'])
	print ('Medium threshold   ', '%4s' % totals['medium'])
	print ('Low threshold      ', '%4s' % totals['low'])
	print ()

#-----------------------------------------------------------------

def sort_results1(a, b):

	# Sort by test index order then file number then line number

	global file_numbers
	
	a_test = Config.tests.index(a['test'])
	b_test = Config.tests.index(b['test'])
	a_file = file_numbers[a['file']]
	b_file = file_numbers[b['file']]
	a_line = a['line1']
	b_line = b['line1']
	
	if a_test < b_test:
		return -1
	elif a_test > b_test:
		return 1
	else:
		if a_file < b_file:
			return -1
		elif a_file > b_file:
			return 1
		else:
			if a_line < b_line:
				return -1
			elif a_line > b_line:
				return 1
			else:
				return 0

#-----------------------------------------------------------------

def sort_results2(a, b):

	# Sort by file number then line number then test index order 

	global file_numbers
	
	a_test = Config.tests.index(a['test'])
	b_test = Config.tests.index(b['test'])
	a_file = file_numbers[a['file']]
	b_file = file_numbers[b['file']]
	a_line = a['line1']
	b_line = b['line1']
	
	if a_file < b_file:
		return -1
	elif a_file > b_file:
		return 1
	else:
		if a_line < b_line:
			return -1
		elif a_line > b_line:
			return 1
		else:
			if a_test < b_test:
				return -1
			elif a_test > b_test:
				return 1
			else:
				return 0

#-----------------------------------------------------------------

class Args():
	pass

#-----------------------------------------------------------------

def parse_args():

	# Set up the command line argument parser

	parser = argparse.ArgumentParser()
	parser.add_argument('input', help='input LaTeX file')
	parser.add_argument('-c', '--config', default='astyle_cfg', help='configuration file')
	parser.add_argument('-r', '--rules', default='rules.txt', help='custom rules file')
#	parser.add_argument('-i', '--ignore', default='', help='custom ignore file')
	parser.add_argument('-o', '--output', default='', help='output file')
#	parser.add_argument('-o', '--option', default='', help='option')
	parser.add_argument('-t', '--threshold', default='low', help="threshold - 'high' (less warnings), 'medium', 'low' (more warnings)")
	parser.add_argument('-v', '--verbose', action='store_true', default=False, help='verbose output - prints the rule criteria')
	parser.add_argument('-l', '--orderbylines', action='store_true', default= False, help='order output by lines (rather than by tests)')
	egroup = parser.add_mutually_exclusive_group()
	egroup.add_argument('-a', '--american', action='store_true', help='American English')
	egroup.add_argument('-b', '--british', action='store_true', help='British English (default)')
	args = parser.parse_args()
#	print (args)

	# Makes all arguments available as Args.argument

#	Args.__dict__.update(vars(args))
	vargs = vars(args)
	for arg in vargs:
		setattr(Args, arg, vargs[arg])
#	Args.__dict__.vars().update(vars(args))
	setattr(sys.modules['__main__'].__builtins__, 'Args', Args)	

	# Makes all configuration variables available as Config.variable

#	print Args.config
	module = __import__(Args.config)
	Config = getattr(module, "Config")
	setattr(sys.modules['__main__'].__builtins__, 'Config', Config)	

	if Args.british:
		Config.british = True
	if Args.american:
		Config.british = False
		
	if Args.output != '':
		sys.stdout = open(Args.output, 'w')
		
#=================================================================

if __name__ == "__main__":
#	print sys.path
	parse_args()
	astyle_main()

#=================================================================
