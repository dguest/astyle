#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------
#
# ATLAS Style Guide Checker
# astyle_config.py - default configuration options
#
# Copyright 2017-22 Steve Lloyd - Queen Mary University of London
# MIT License
#
#-----------------------------------------------------------------

import re

class ConfigBase():
	# General options
	
	version = '0.1'
	british = True			# If true - British English - else American English
	context_max = 15		# Maximum characters in context either side of a match
	threshold_levels = {'high': 3, 'medium': 2, 'low': 1}
	
	# Ignore these acronyms

	ignore_acronyms = ['CERN', 'ATLAS', 'CMS', 'ATLASLATEXPATH', 'PAPER', 'LHC', 'LANGEDIT', 'LANGSHOW', 'SHERPA', 'SHERPAV', 'POWHEG', 'POWHEGV', 'POWHEGBOX', 'POWHEGBOXV', 'PYTHIA', 'PYTHIAV', 'POWPYTHIA', 'PROTOS', 'HERWIG', 'HERWIGV', 'GEANT', 'URL', 'JHEP', 'EPJC', 'PRD', 'LUCID', 'MADGRAPH', 'MADGRAPHV', 'ANA', 'PETRA', 'PEP', 'LEP', 'SLC', 'HERA', 'RHIC']

	# Ignore acronyms inside these files

	ignore_acronym_files = ['Acknowledgements.tex', 'acknowledgements.tex']

	# Ignore these files

	ignore_files = ['Acknowledgements.tex', 'acknowledgements.tex', 'auxiliary.tex']

	# Ignore everything inside \command{...}
	
	ignored_latex_commands = ['ref', 'href', 'url', 'input', 'bibliography', 'cite', 'autocite', 'affil', 'nocite', 'cline', 'newcommand', 'includegraphics', 'label', 'usepackage', 'num', 'AtlasCoverSupportingNote', 'AtlasCoverEgroupEditors', 'AtlasCoverEgroupEdBoard', 'AtlasRefCode', 'AtlasAuthorContributor', 'addbibresource']

	standard_references = [('The ATLAS Detector', 'PERF-2007-01'), ('Geant4', 'Agostinelli:2002hh'), ('ATLAS Simulation', 'SOFT-2010-01'), ('FastJet', 'Cacciari:2011ma')]
	
	rules_file = 'astyle_rules.txt'
	rule_names = {'phrase': 'Phrases', 'spelling': 'Spelling', 'syntax': 'Syntax', 'capitalize': 'Capitalize', 'hyphenate': 'Hyphenate', 'english': 'English'}
		
	# This defines the order the tests are run in:
	# If tests is empty it is automatically filled with all the tests
	
	tests = []	
#	tests = ['English']
	ignore_tests = []
		
	# This defines the tests themselves
	# test_data is a list of terms to check. 
	# Each term can be a string or a tuple (term, suggestion).
	# phase 1 is everything.
	# phase 2 is after removal of equations.
	# phase 3 is after removal of tables.
	# ASG expands to "The ATLAS Style Guide says - "
	
	# (?=...)	Positive lookahead
	# (?<=...)	Positive lookbehind
	# (?!...)	Negative lookahead
	# (?<!...)	Negative lookbehind
	
	test_data = [
			{'name': 'Which v That', 'run': True, 'function': 'findregexterm', 'case': False, 'terms': [r'(?<!,)(?<!in)(?<!at)(?<!iof)(?<!for)\s(which)\s', r'(,\sthat\s)'], 'desc': "In a defining clause, use that, in non-defining clauses, use which - 'The cat that sat on the mat was black, which was unuusal'.", 'level': 'high', 'phase': 1},
			{'name': 'Line Breaks', 'run': True, 'function': 'findregexterm', 'case': False, 'terms': [r'(\\\\|\\newline)'], 'desc': "Check line break - use a blank line for a new paragraph.", 'level': 'high', 'phase': 3},
			{'name': 'English', 'run': True, 'function': 'findenglish', 'case': False, 'aterms': [], 'bterms': [], 'desc': "ASG British English should be used for European journals and American English should be used for American journals (PRC, PRD, and PRL).", 'level': 'high', 'phase': 1},
			{'name': 'Fig Sect Tab', 'run': True, 'function': 'findregexterm', 'case': False, 'terms': [r'\b(Fig[\s\.])', r'\b(Sect[\s\.])', r'\b(Tab[\s\.])'], 'desc': "ASG Always use the full word 'Figure', not 'Fig.', and capitalize it.and when referring to a section in the text, write out the word in full and capitalized, e.g. 'This is discussed in Section 4.3'. Do not use 'Sect.'. There is no abbreviation for the word 'Table'.", 'level': 'high', 'phase': 1},
			{'name': 'Symbol Start', 'run': True, 'function': 'findregexterm', 'case': False, 'terms': [r'^(\$.+?\$\s+)', r'(?<!i\.e)(?<!e\.g)\.\s+(\$.+?\$\s+)'], 'desc': "ASG Do not start a sentence with a symbol - it is not good style", 'level': 'high', 'phase': 1},
			{'name': 'MeV GeV TeV', 'run': True, 'function': 'findregexterm', 'case': False, 'terms': [r'([^\\](?<!\\si\{)MeV)\b', r'([^\\](?<!\\si\{)GeV)\b', r'([^\\](?<!\\si\{)TeV)\b', r'(\\,MeV)\b', r'(\\,GeV)\b', r'(\\,TeV)\b'], 'desc': "ASG For energy units, use the \MeV, \GeV, and \TeV macros. They include a typographical spacing correction for the unusual 'eV' character combination.", 'level': 'high', 'phase': 1},
			{'name': 'Acronym Plurals', 'run': True, 'function': 'findregexterm', 'case': True, 'terms': [r'\b([A-Z]{2,}\'s)\b'], 'desc': "ASG Do not use apostrophes to indicate plural acronyms, e.g. RODs not ROD's.", 'level': 'high', 'phase': 1},
			{'name': 'Personal Pronouns', 'run': True, 'function': 'findsimpleterm', 'case': True, 'terms': ['I', '[Yy]ou', '[Hh]e', '[Ss]he', '[Ww]e', '[Mm]e', '[Hh]im', '[Hh]er', '[Uu]s'], 'desc': "ASG Avoid the use of personal pronouns.", 'level': 'high', 'phase': 1},
			{'name': 'ATLAS Jargon', 'run': True, 'function': 'findsimpleterm', 'case': False, 'terms': ['-like'], 'desc': "ASG Avoid using ATLAS-specific and LHC-specific jargon and Examples of jargon that should be avoided are: 'Higgs-like', 'SM-like', or 'monojet-like'.", 'level': 'high', 'phase': 1},
			{'name': 'QCD Background', 'run': True, 'function': 'findsimpleterm', 'case': False, 'terms': ['QCD background'], 'desc': "ASG Background events from jet production is often referred to as 'QCD background'. Since QCD is the theory of strong interactions rather than a source of background, such events should be referred to as 'multijet background'.", 'level': 'high', 'phase': 1},
			{'name': 'Charged Tracks', 'run': True, 'function': 'findsimpleterm', 'case': False, 'terms': ['charged track', 'charged tracks'], 'desc': "ASG Avoid 'charged tracks' and write 'charged-particle tracks' instead.", 'level': 'high', 'phase': 1},
			{'name': 'Calorimeter Cluster', 'run': True, 'function': 'findsimpleterm', 'case': False, 'terms': ['calorimeter cluster', 'calorimeter clusters'], 'desc': "ASG A 'calorimeter cluster' is a cluster of calorimeters. Viable alternatives are 'cluster of energy deposits' or 'calorimeter energy clusters'.", 'level': 'high', 'phase': 1},
			{'name': 'Plural Particles', 'run': True, 'function': 'findregexterm', 'case': False, 'terms': ['(\$.+?\$\'s)'], 'desc': "ASG Avoid using an apostrophe for plurals of particles as in 'π0's', use 'π0s' instead.", 'level': 'high', 'phase': 1},
			{'name': 'Stop Quark', 'run': True, 'function': 'findsimpleterm', 'case': False, 'terms': ['stop quark'], 'desc': "ASG Squark names should be: 'top squark', 'bottom squark' etc. It is fine to define 'stop' as a shorthand for 'top squark' but 'stop quark' should not be used.", 'level': 'high', 'phase': 1},
			{'name': 'Higgs Boson', 'run': True, 'function': 'findregexterm', 'case': False, 'terms': [r'\b(higgs(?!\s+boson)(?!\s+doublet)(?!\s+sector)(?!\s+triplet)(?!-doublet))\b'], 'desc': "ASG The boson is simply the 'Higgs boson' (and not just 'the Higgs') when referring to the Standard Model Higgs boson.", 'level': 'medium', 'phase': 1},
			{'name': 'Boson Fusion', 'run': True, 'function': 'findsimpleterm', 'case': False, 'terms': ['[^-]gluon fusion', 'weak boson fusion', 'gluon-fusion', 'gluon--fusion', 'weak-boson-fusion', 'weak--boson-fusion', 'weak-boson--fusion', 'weak--boson--fusion'], 'desc': "ASG Higgs boson production processes should be written as 'gluon--gluon fusion' not 'gluon fusion', and 'vector-boson fusion (VBF)' not 'weak-boson fusion'.", 'level': 'high', 'phase': 1},
			{'name': 'Tunes', 'run': True, 'function': 'findsimpleterm', 'case': False, 'terms': ['tunes'], 'desc': "ASG MC tunes is jargon, introduce them as tuned parameters of the Monte Carlo programs.", 'level': 'high', 'phase': 1},
			{'name': 'Powheg', 'run': True, 'function': 'findsimpleterm', 'case': True, 'terms': ['PowHeg'], 'desc': "ASG Use ‘Powheg’ or ’POWHEG’, never ‘PowHeg’. Distinguish between the ‘POWHEG’ method (implemented in many Monte Carlo programs) and the ‘POWHEG-BOX’ program.", 'level': 'high', 'phase': 1},
			{'name': 'Orders', 'run': True, 'function': 'findsimpleterm', 'case': False, 'terms': ['note that'], 'desc': "ASG Avoid giving orders as in 'Note that'. The meaning is usually precisely the same without this instruction.", 'level': 'high', 'phase': 1},
			{'name': 'Minus Signs', 'run': True, 'function': 'findregexterm', 'case': False, 'terms': [r'(\s-{1}\d)', r'(^-{1}\d)'], 'desc': "ASG Short hyphens should not be used for minus signs (use mathmode minus sign, or an en-dash instead).", 'level': 'high', 'phase': 2},
			{'name': 'Truth', 'run': True, 'function': 'findsimpleterm', 'case': False, 'terms': ['truth', 'closure'], 'desc': "ASG Avoid such ATLAS-specific jargon as 'truth particle' (and 'truth' in general)", 'level': 'high', 'phase': 1},
			{'name': 'Cross section', 'run': True, 'function': 'findsimpleterm', 'case': False, 'terms': ['cross section', 'cross sections'], 'desc': "ASG cross-section is recommended (this is controversial: hyphenation is preferred, but if unhyphenated 'cross section' is chosen do it consistently and hyphenate when used as a compound adjective)", 'level': 'medium', 'phase': 1},
			{'name': 'Percentage Space', 'run': True, 'function': 'findregexterm', 'case': False, 'terms': [r'(\d[\s~]\%)'], 'desc': "ASG For percentages, do not include a space between the number and the percent sign.", 'level': 'high', 'phase': 1},
			{'name': 'Confidence Level', 'run': True, 'function': 'findsimpleterm', 'case': False, 'terms': ['confidence limit', 'credibility limit'], 'desc': "ASG The terms 'confidence level' (frequentist/CLs) or credibility level (Bayesian) can be abbreviated to 'CL' (but define it on first use); note that it is 'level' not 'limit'.", 'level': 'high', 'phase': 1},
			{'name': 'C.l.', 'run': True, 'function': 'findregexterm', 'case': True, 'terms': [r'\b(C\.L\.)'], 'desc': "ASG The terms 'confidence level' (frequentist/CLs) or credibility level (Bayesian) can be abbreviated to 'CL' (but define it on first use). Writing it as 'C.L.' is discouraged.", 'level': 'high', 'phase': 1},
			{'name': 'Generic Lepton', 'run': True, 'function': 'findregexterm', 'case': True, 'terms': [r'(\$l\$)'], 'desc': "ASG Always use a cursive $\ell$ for a generic lepton (usually $e$ or $\mu$), not $l$.", 'level': 'high', 'phase': 1},
			{'name': 'Allows to Measure', 'run': True, 'function': 'findsimpleterm', 'case': False, 'terms': ['allows to measure'], 'desc': "ASG 'Allows to measure' should be something like 'allows measurement of'.", 'level': 'high', 'phase': 1},
			{'name': 'Allows For', 'run': True, 'function': 'findsimpleterm', 'case': False, 'terms': ['allows for'], 'desc': "ASG Use 'allows', not 'allows for', if you mean something is permitted or enabled. For example, 'doing this allows changes due to ageing to be estimated'. 'Allows for' means you have considered something that is possible but not certain, for example 'the error quoted allows for possible changes due to ageing'.", 'level': 'medium', 'phase': 1},
			{'name': 'Perfect Tense', 'run': True, 'function': 'findsimpleterm', 'case': False, 'terms': ['has been', 'had been', 'have been'], 'desc': "ASG Do not use tenses such as the past perfect (e.g. 'had been') or present perfect (e.g. 'has been' or 'have been') when the simple past or present (e.g. 'was' or 'is') are appropriate. For example, 'This efficiency was found to be 0.85' rather than 'This efficiency has been found to be 0.85'. This is more direct and immediate.", 'level': 'high', 'phase': 1},
			{'name': 'Discussed', 'run': True, 'function': 'findsimpleterm', 'case': False, 'terms': ['this will be discussed'], 'desc': "ASG Do not write phrases like 'this will be discussed in Section 5.2' - it is discussed there now, so just say 'this is discussed in Section 5.2'.", 'level': 'high', 'phase': 1},
			{'name': 'Decay To', 'run': True, 'function': 'findsimpleterm', 'case': False, 'terms': ['decay(s|ed|ing)?\s*\w*\s+to'], 'desc': "The Oxford Dictionary says - The W-particle then decays into an electron and a neutrino, which is distinct from 'decay' as a 'gradual decrease' as in 'the time taken for the current to decay to zero'.", 'level': 'high', 'phase': 1},
			{'name': 'Colours', 'run': True, 'function': 'findsimpleterm', 'case': False, 'terms': ['(red|green|blue|yellow|pink|grey|gray|black|orange|brown)'], 'desc': "ASG The caption and text should not mention colours, nor require any colour distinctions in order to interpret the ﬁgure (e.g. 'the red curve corresponds to ...').", 'level': 'high', 'phase': 1},
			{'name':'Axes' , 'run': True, 'function': 'findregexterm', 'case': False, 'terms': [r'(\$[xyz]\$\s+axis)\b', r'\b([xyz]\-axis)\b', r'\b([xyz]\s+axis)\b'], 'desc': "Use $x$\-axis not $x$ axis or y axis or z-axis.", 'level': 'high', 'phase': 1},
			{'name': 'Single Letter Hyphens', 'run': True, 'function': 'findregexterm', 'case': True, 'terms': [r'(\$[udscbt]\$\s+\w+)', r'\s+([udscb]\s+(?![lrc])\w+)', r'(\$[AHZW]{1}\$-\w+)', r'\s+([AHZW]-\w+)'], 'desc': "ASG The $b$-quark should be hyphenated but not $W$ boson.", 'level': 'high', 'phase': 1},
			{'name': 'No Hyphen', 'run': True, 'function': 'findregexterm', 'case': False, 'terms': [r'(\w+ly-\w+)'], 'desc': "ASG One case where a string of modifiers does not take a hyphen is when the modifier is an adverb, most notably ending in -ly. That is because it is clear what the adverb is modifying. For example, 'badly written paper' or 'newly discovered particle", 'level': 'high', 'phase': 1},
			{'name': 'Prefixes and Suffixes', 'run': True, 'function': 'findregexterm', 'case': False, 'terms': [r'\b(di-jets)\b', r'\b(di-boson[s]?)\b', r'\b(re-entry)\b', r'\b(anti-quark[s]?)\b', r'\b(anti-matter)\b', r'\b(re-normalisation)\b', r'\b(re-normatlization)\b', r'\b(wire-less)\b', r'\b(?!antimatter)(?!antiquark)(anti\w+)\b', r'\b(?!wireless)(?!massless)(?!unless)(?!stainless)(?!dimensionless)(\w+(like|less)+)\b', r'\b((?!multiple)(?!multiply)(?!multiplied)(?!multiplicative)(?!multiplicity)(?!multiplicities)(?!multivariate)(?!multiplication)(?!multiplication)(?!multiplies)(?!multijet)(?!Multijet)(?!multipurpose)(?!none)(multi|quasi|non)+\w+)\b'], 'desc': "ASG There are a number of preﬁxes and suﬃxes that are joined to words with hyphens, and in many cases the words pass into standard usage and lose the hyphen (but sometimes not everyone will agree) - anti-anything, but antiquark and antimatter; dilepton, diboson, dijets (not everyone agrees); multi-anything, non-anything, quasi-anything; n-type, p-type; reentry, re-analyse, but renormalization, anything-like, anything-less, but wireless.", 'level': 'high', 'phase': 1},
			{'name': 'Non-breaking Space', 'run': True, 'function': 'findregexterm', 'case': False, 'terms': [r'(\w+\s+\\cite{)'], 'desc': "ASG A non-breaking space '~' between 'Ref.' and the number keeps them together on the same line.", 'level': 'medium', 'phase': 1},
			{'name': 'Generators', 'run': True, 'function': 'findregexterm', 'case': False, 'terms': [r'({\\tt\s+.*?})', r'(\\textsc{.*?})', r'\b(GEANT)'], 'desc': "Most Monte Carlo generators have a macro like \PYTHIA and also have a form with a suffix 'V' that allows you to include the version, e.g. \PYTHIAV{8} to produce Pythia 8 or \PYTHIAV{8 (v8.160)}to produce Pythia8(v8.160).", 'level': 'low', 'phase': 1},
			{'name': 'Tau Lepton', 'run': True, 'function': 'findregexterm', 'case': False, 'terms': [r'\b(tau)\b', r'(\$\\tau\$)(?!-lepton)'], 'desc': "Note that the recommended use for the third-generation charged lepton is 'τ-lepton', rather than 'tau', 'tau lepton' or just 'τ'.", 'level': 'high', 'phase': 1},
			{'name': 'Numbers', 'run': True, 'function': 'findregexterm', 'case': False, 'terms': [r'(?<!Run)\s(\d)\s', r'(\$\d\$)', r'\b((?<!\.)(?<!/)\d{5,}\b)'], 'desc': "Write out integers from one to ten. Use digits for decimals, percentages, and when there are units. Use spaces for numbers with five or more digits. Numbers at the beginning of a sentence should always be written out. Do not mix digits and words - use digits.", 'level': 'medium', 'phase': 3},
			{'name': 'References', 'run': True, 'function': 'findregexterm', 'case': False, 'terms': [r'[.?!]\s+(Ref)\b', r'\w+\s+(Reference)', r'(are found in[~\s]+\\cite{)'], 'desc': "At the beginning of a sentence use the full word 'Reference'. Within a sentence, the abbreviation 'Ref.' should be used and the phrasing should be as in 'details are found in Ref. [1]' rather than 'details are found in [1]' but don't use 'Ref.' before other reference citations.", 'level': 'high', 'phase': 1},
			{'name': 'Standard References', 'run': True, 'function': '', 'terms': [], 'desc': "Include references to the following papers: ATLAS detector, Geant4, ATLAS simulation and FastJet package, unless not relevant to the paper.", 'level': 'high', 'phase': 1},
			{'name': 'Acronyms', 'run': True, 'function': '', 'terms': [], 'desc': "Do not use too many acronyms. Check whether you are really saving much space, and not just using them once or twice. ", 'level': 'medium', 'phase': 2},
			{'name': 'Phrases', 'run': True, 'function': 'findsimpleterm', 'case': False, 'terms': [], 'desc': "Miscellaneous phrasing that could be improved.", 'level': 'medium', 'phase': 2},
			{'name': 'Spelling', 'run': True, 'function': 'findsimpleterm', 'case': False, 'terms': [], 'desc': "Common misspellings or typos.", 'level': 'medium', 'phase': 2},
			{'name': 'Syntax', 'run': True, 'function': 'findregexterm', 'case': True, 'terms': [], 'desc': "Wrong syntax.", 'level': 'low', 'phase': 2},
			{'name': 'Capitalize', 'run': True, 'function': 'findsimpleterm', 'case': True, 'terms': [], 'desc': "Expressions or words that should or shouldn't be capitalized.", 'level': 'medium', 'phase': 2},
			{'name': 'Hyphenate', 'run': True, 'function': 'findsimpleterm', 'case': False, 'terms': [], 'desc': "ASG Try to minimize the number of hyphens by using them only where necessary. Use a hyphen for compound adjectives if the meaning would be ambiguous without a hyphen ... For example, use 'single-top-quark samples' instead of 'single top quark samples', as the latter could be interpreted as single samples of top-quark events.", 'level': 'high', 'phase': 1},
	]
	
	# This is filled automatically from test_data
	
	test_info = {}
	